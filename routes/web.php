<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BulletinController;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [BulletinController::class, 'index'])->name('index');
Route::post('/store', [BulletinController::class, 'store'])->name('store');

Route::prefix('{bulletin}')->group(function () {
    Route::post('/confirm-password', [BulletinController::class, 'confirmPassword'])->name('confirm-password');
    Route::get('/delete', [BulletinController::class, 'confirmDelete'])->name('confirm-delete');
    Route::delete('/destroy',[BulletinController::class, 'destroy'])->name('delete');
    Route::get('/edit', [BulletinController::class, 'edit'])->name('edit');
    Route::post('/update', [BulletinController::class, 'update'])->name('update');
});

Auth::routes(['verify' => true]);

Route::post('/register-confirm', 'Auth\RegisterController@confirmation')->name('register-confirm');
Route::view('/verify/success','auth.verify-success')->middleware(['auth', 'verified'])->name('verification-success');

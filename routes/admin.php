<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\Auth\LoginController;

/*
* Admin Routes
*
*/

Route::prefix('admin')->group(function () {
    Route::get('/login', [LoginController::class, 'showLoginForm'])->name('admin.login');
    Route::post('/login', [LoginController::class, 'login']);
    Route::post('/logout', [LoginController::class, 'logout'])->name('admin.logout');
    
    Route::middleware('admin')->group(function () {
        Route::get('/', [DashboardController::class, 'index'])->name('admin.index');

        Route::prefix('{bulletin}')->group(function () {
            Route::post('/delete-image',[DashboardController::class, 'deleteImage'])->name('admin.delete-image');
            Route::delete('/destroy',[DashboardController::class, 'destroy'])->name('admin.delete');
        });
        
        Route::get('/{id}/restore', [DashboardController::class, 'restore'])->name('admin.restore');
        Route::delete('/delete-multiple', [DashboardController::class, 'deleteMultiple'])->name('admin.delete-multiple');
    });
    
});
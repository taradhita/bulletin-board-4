@props(['bulletin', 'page'])

<div class="post">
    <div class="clearfix">
        <div class="pull-left">
            <h2 class="mb-5 text-green"><b>{{ $bulletin->title }}</b></h2>
        </div>
        <div class="pull-right text-right">
            <p class="text-lgray"> {{ customDate($bulletin->created_at, 'd-m-Y') }}<br /><span class="small">{{ customDate($bulletin->created_at, 'H:i') }}</span></p>
        </div>
    </div>
    <h4 class="mb-20">
        {{ $bulletin->name }} 
        @if ($bulletin->user_id)
            <span class="text-id">[ID: {{$bulletin->user_id}}]</span>
        @endif
    </h4>
    <p>{!! nl2br($bulletin->message) !!}</p>
    <div class="img-box my-10">
        @if($bulletin->image_thumbnail)
        <img class="img-responsive img-post" src="{{ $bulletin->image_thumbnail }}" alt="image">
        @endif
    </div>
    <form class="form-inline mt-50" method="post" action="{{ route('confirm-password', $bulletin->id) }}" id="passwordForm">
        @csrf
        <div class="form-group mx-sm-3 mb-2">
            <input type="hidden" name="itemsCount" value="{{ count($page->data) }}">
            @guest
                @if(empty($bulletin->user_id))
                    <label for="inputPassword2" class="sr-only">Password</label>
                    <input type="password" class="form-control" placeholder="Password" name="passwordConfirmation" id="passwordConfirmId{{$bulletin->id}}">
                @endif
            @else
                @if(Auth::user()->id == $bulletin->user_id)
                    <input type="hidden" name="passwordConfirmation" id="passwordConfirmId{{$bulletin->id}}" value="{{ Auth::user()->password }}"> 
                @endif
            @endguest
        </div>
        @guest 
            @if (empty($bulletin->user_id))
                <button type="submit" class="btn btn-default mb-2" name="action" value="edit" id ="edit" data-id="{{$bulletin->id}}"><i class="fa fa-pencil p-3"></i></button>
                <button type="submit" id="delete" class="btn btn-danger mb-2" name="action" value="delete" data-id="{{$bulletin->id}}"><i class="fa fa-trash p-3"></i></button>
            @endif
        @else
            @if (Auth::user()->id == $bulletin->user_id)
                <button type="submit" class="btn btn-default mb-2" name="action" value="edit" id ="edit" data-id="{{$bulletin->id}}"><i class="fa fa-pencil p-3"></i></button>
                <button type="submit" id="delete" class="btn btn-danger mb-2" name="action" value="delete" data-id="{{$bulletin->id}}"><i class="fa fa-trash p-3"></i></button>
            @endif
        @endguest
    </form>
</div>
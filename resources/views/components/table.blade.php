<table class="table {{ $class }}">
    @if(isset($header))
        <thead>
            {{ $header }}
        </thead>
    @endif
    <tbody>
        {{ $slot }}
    </tbody>
</table>
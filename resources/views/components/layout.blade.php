<html>

<head>
    <title>Timedoor Challenge - Level 8</title>
    <meta name="_token" content="{{csrf_token()}}">
    @include('template/style')
    @include('template/script')
</head>

<body class="bg-lgray">
    <header>
        @include('template/header')
    </header>

    <main>
        {{$slot}}
    </main>

    <footer>
        @include('template/footer')
    </footer>

    @include('modal/edit')
    @include('modal/delete')
    <script type="text/javascript" src="{{ asset('js/ajax.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/input-file.js') }}"></script>
</body>

</html>
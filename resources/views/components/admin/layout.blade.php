<?php
$page = 'home';
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="_token" content="{{csrf_token()}}">
    <title>Timedoor Admin | Dashboard</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    @include('admin/template/style')
</head>

<body class="hold-transition skin sidebar-mini">
    <div class="wrapper">

        <x-admin.header />
        
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            @include('admin/template/sidebar')
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">

            {{ $slot }}

        </div>
        <!-- /.content-wrapper -->
        <x-admin.footer />

        @include('admin/modal/delete-item')
        @include('admin/modal/delete-image')
        @include('admin/modal/validation')

    </div>

    @include('admin/template/script')

    <script>
        // BOOTSTRAP TOOLTIPS
        if ($(window).width() > 767) {
            $(function() {
                $('[rel="tooltip"]').tooltip()
            });
        };
    </script>
    <!-- Check all checkbox -->
    <script type="text/javascript" src="{{ asset('admin_assets/js/clickAll.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin_assets/js/ajax.js') }}"></script>
</body>

</html>
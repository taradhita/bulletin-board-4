<div class="bordered-box mb-20">
    <form class="form" role="form">
        <x-table class="no-border mb-0">
            <tr>
                <td width="80"><b>Title</b></td>
                <td>
                    <div class="form-group mb-0">
                        <input type="text" class="form-control" name="title" value="{{ request('title') }}">
                    </div>
                </td>
            </tr>
            <tr>
                <td><b>Body</b></td>
                <td>
                    <div class="form-group mb-0">
                        <input type="text" class="form-control" name="body" value="{{ request('body') }}">
                    </div>
                </td>
            </tr>
        </x-table>
        <x-table class="table-search">
            <tr>
                <td width="80"><b>Image</b></td>
                <td width="60">
                    <label class="radio-inline">
                        <input type="radio" name="imageOption" id="inlineRadio1" value="yes" {{ (request('imageOption') == 'yes') ? 'checked' : '' }}> with
                    </label>
                </td>
                <td width="80">
                    <label class="radio-inline">
                        <input type="radio" name="imageOption" id="inlineRadio2" value="no" {{ (request('imageOption') == 'no') ? 'checked' : '' }}> without
                    </label>
                </td>
                <td>
                    <label class="radio-inline">
                        <input type="radio" name="imageOption" id="inlineRadio3" value="" {{ (request('imageOption') == '') ? 'checked' : '' }}> unspecified
                    </label>
                </td>
            </tr>
            <tr>
                <td width="80"><b>Status</b></td>
                <td>
                    <label class="radio-inline">
                        <input type="radio" name="statusOption" id="inlineRadio1" value="yes" {{ (request('statusOption') == 'yes') ? 'checked' : '' }}> on
                    </label>
                </td>
                <td>
                    <label class="radio-inline">
                        <input type="radio" name="statusOption" id="inlineRadio2" value="no" {{ (request('statusOption') == 'no') ? 'checked' : '' }}> delete
                    </label>
                </td>
                <td>
                    <label class="radio-inline">
                        <input type="radio" name="statusOption" id="inlineRadio3" value="" {{ (request('statusOption') == '') ? 'checked' : '' }}> unspecified
                    </label>
                </td>
            </tr>
            <tr>
                <td><button type="submit" class="btn btn-default mt-10"><i class="fa fa-search"></i> Search</button></td>
            </tr>
        </x-table>
    </form>
</div>
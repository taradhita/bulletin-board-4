@props(['bulletin'])

@if($bulletin->deleted_at)
    <tr class="bg-gray-light">
        <td>&nbsp;</td>
        <td>{{$bulletin->id}}</td>
        <td>{{$bulletin->title}}</td>
        <td>{{$bulletin->message}}</td>
        <td>
            -
        </td>
        <td>{{ customDate($bulletin->created_at, 'Y/m/d') }}<br><span class="small">{{ customDate($bulletin->created_at,'H:i:s') }}</span></td>
        <td><a href="{{ route('admin.restore', ['id' => $bulletin->id]) }}" class="btn btn-default" id="recoverItem" rel="tooltip" title="Recover"><i class="fa fa-repeat"></i></a></td>
    </tr>
    @else
    <tr>
        <td><input type="checkbox" class="checkboxItem" data-id="{{$bulletin->id}}"></td>
        <td>{{$bulletin->id}}</td>
        <td>{{$bulletin->title}}</td>
        <td>{{$bulletin->message}}</td>
        <td>
            <img class="img-prev" src="{{$bulletin->image_thumbnail}}">
            <a href="#" data-toggle="modal" data-target="#deleteImageModal" class="btn btn-danger ml-10 btn-img" id="deleteImage" rel="tooltip" title="Delete Image" data-id="{{$bulletin->id}}"><i class="fa fa-trash"></i></a>
        </td>
        <td>{{date('Y/m/d', strtotime($bulletin->created_at))}}<br><span class="small">{{ date('H:i:s', strtotime($bulletin->created_at)) }}</span></td>
        <td><a href="#" data-toggle="modal" data-target="#deleteModal" class="btn btn-danger" id="deleteItem" rel="tooltip" title="Delete" data-id="{{$bulletin->id}}"><i class="fa fa-trash"></i></a></td>
    </tr>
@endif
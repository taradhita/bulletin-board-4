@props(['bulletins'])

<x-table class="table-bordered">
    <x-slot name="header">
        <tr>
            <th><input type="checkbox" onclick="clickAll(this)"></th>
            <th>ID</th>
            <th>Title</th>
            <th>Body</th>
            <th width="200">Image</th>
            <th>Date</th>
            <th width="50">Action</th>
        </tr>
    </x-slot>
    @foreach($bulletins as $bulletin)
        <x-admin.bulletin :bulletin="$bulletin" />
    @endforeach
</x-table>
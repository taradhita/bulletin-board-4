<x-layout>
    <div class="section">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 bg-white p-30 box">
                    <div class="text-center">
                        <h1 class="text-green mb-30"><b>Level 8 Challenge</b></h1>
                    </div>
                    @if(session()->has('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div>
                    @endif
                    <form method="post" action="{{ route('store') }}" id="formCreate" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label>Name</label>
                            @auth
                                <input type="text" class="form-control" name="name" readonly value="{{ Auth::user()->name }}">
                            @else
                                <input type="text" class="form-control" name="name" value="{{ old('name') ?? '' }}">
                                @error('name')
                                    <p class="small text-danger mt-5">*{{ $message }}</p>
                                @enderror
                            @endauth
                        </div>
                        <div class="form-group">
                            <label>Title</label>
                            <input type="text" class="form-control" name="title" value="{{ old('title') ?? '' }}">
                            @error('title')
                                <p class="small text-danger mt-5">*{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label>Body</label>
                            <textarea rows="5" class="form-control" name="message">{{ old('message') ?? '' }}</textarea>
                            @error('message')
                                <p class="small text-danger mt-5">*{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label>Choose image from your computer :</label>
                            <div class="input-group">
                                <input type="text" class="form-control upload-form" value="No file chosen" readonly>
                                <span class="input-group-btn">
                                    <span class="btn btn-default btn-file">
                                        <i class="fa fa-folder-open"></i>&nbsp;Browse <input type="file" name="image" multiple>
                                    </span>
                                </span>
                            </div>
                            @error('image')
                                <p class="small text-danger mt-5">*{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="form-group">
                        @guest
                            <label>Password</label>
                            <input type="password" class="form-control" name="password">
                            @error('password')
                                <p class="small text-danger mt-5">*{{ $message }}</p>
                            @enderror
                        @endguest
                        </div>
                    </form>
                    <div class="text-center mt-30 mb-30">
                        <button class="btn btn-primary" type="submit" form="formCreate" value="Submit">Submit</button>
                    </div>
                    <hr>
                    @if($bulletins->count())
                        @foreach($bulletins as $bulletin)
                            <x-bulletin-item :bulletin="$bulletin" :page="$page" />
                        @endforeach
                        <div class="text-center mt-30">
                            {{ $bulletins->links('vendor.pagination.custom-pagination') }}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</x-layout>
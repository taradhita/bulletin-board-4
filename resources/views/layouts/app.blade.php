<html>
    <head>
        <title>Timedoor Challenge - Level 8 | @yield('title')</title>

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- CSS -->
        <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset ('css/style.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/tmdrPreset.css') }}">
        <!-- CSS End -->

        <!-- Javascript -->
        <script type="text/javascript" src="{{ asset('js/jquery.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
        <!-- Javascript End -->
    </head>

    <body id="login">
        @yield('content')
    </body>
</html>
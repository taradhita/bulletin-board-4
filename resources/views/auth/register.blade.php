@extends('layouts.app')

@section('title', 'Register')

@section('content')
    <div class="box login-box">
        <div class="login-box-head">
            <h1 class="mb-5">Register</h1>
            <p class="text-lgray">Please fill the information below...</p>
        </div>
        <form method="POST" action="{{ route('register') }}">
            @csrf
            <div class="login-box-body">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Name" name="name" value="{{ old('name') ?? '' }}">
                    @error('name')
                    <p class="mt-5 small text-danger">*{{ $message }}</p>
                    @enderror
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="E-mail" name="email" value="{{ old('email') ?? '' }}">
                    @error('email')
                    <p class="mt-5 small text-danger">*{{ $message }}</p>
                    @enderror
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" placeholder="Password" name="password" value="{{ old('password') ?? '' }}">
                    @error('password')
                    <p class="mt-5 small text-danger">*{{ $message }}</p>
                    @enderror
                </div>
            </div>
            <div class="login-box-footer">
                <div class="text-right">
                    <a href="/" class="btn btn-default">Back</a>
                    <button type="submit" class="btn btn-primary">Confirm</button>
                </div>
            </div>
        </form>
    </div>
@endsection
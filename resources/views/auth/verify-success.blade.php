@extends('layouts.app')

@section('content')
    <div class="box login-box text-center">
        <div class="login-box-head">
            <h1>Successfully Registered</h1>
        </div>
        <div class="login-box-body">
            {{ __('Thank you for your registration. Membership is now complete.') }}
        </div>
        <div class="login-box-footer">
            <div class="text-center">
                <a href="/" class="btn btn-primary">Back to Home</a>
            </div>
        </div>
    </div>
@endsection
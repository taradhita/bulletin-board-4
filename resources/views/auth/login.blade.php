@extends('layouts.app')

@section('title', 'Login')

@section('content')
    <div class="box login-box">
        <div class="login-box-head">
            <h1 class="mb-5">Login</h1>
            <p class="text-lgray">Please login to continue...</p>
        </div>
        <form method="POST" action="{{ route('login') }}">
            @csrf
            <div class="login-box-body">
                <div class="form-group">
                    <input type="text" class="form-control" name="email" placeholder="Email" value="{{ old('email') ?? '' }}">
                    @error('email')
                    <p class="mt-5 small text-danger">*{{ $message }}</p>
                    @enderror
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" name="password" placeholder="Password" value="{{ old('password') ?? '' }}">
                    @error('password')
                    <p class="mt-5 small text-danger">*{{ $message }}</p>
                    @enderror
                </div>
            </div>
            <div class="login-box-footer">
                <div class="text-right">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </form>
    </div>

@endsection
@extends('layouts.app')

@section('content')
    <div class="box login-box text-center">
        <div class="login-box-head">
            <h1>Successfully Registered</h1>
        </div>
        <div class="login-box-body">
            <p> Thank you for your membership register. <br />
                We send confirmation e-mail to you. Please complete the registration by clicking the confirmation URL. </p><br />
            {{ __('If you did not receive the email') }},
            <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                @csrf
                <button type="submit" class="btn btn-link p-0 m-0 align-baseline">{{ __('click here to request another') }}</button>.
            </form>
        </div>
        <div class="login-box-footer">
            <div class="text-center">
                <a href="/" class="btn btn-primary">Back to Home</a>
            </div>
        </div>
    </div>
@endsection
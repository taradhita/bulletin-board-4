@extends('layouts.app')

@section('content')
    <div class="box login-box">
        <div class="login-box-head">
            <h1>Register</h1>
        </div>
        <form method="POST" action="{{ route('register-confirm') }}">
            @csrf
            <div class="login-box-body">
                <table class="table table-no-border">
                    <tbody>
                        @if($data)
                        <tr>
                            <th>Name</th>
                            <td>{{ $data['name'] }}</td>
                            <input type="hidden" name="name" value="{{ $data['name'] }}">
                        </tr>
                        <tr>
                            <th>E-mail</th>
                            <td>{{ $data['email'] }}</td>
                            <input type="hidden" name="email" value="{{ $data['email'] }}">
                        </tr>
                        <tr>
                            <th>Password</th>
                            <td> {{ $data['password'] }} </td>
                            <input type="hidden" name="password" value="{{ $data['password'] }}">
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            <div class="login-box-footer">
                <div class="text-right">
                    <button type="submit" type="submit" class="btn btn-primary">Submit</a>
                </div>
            </div>
        </form>
    </div>
@endsection
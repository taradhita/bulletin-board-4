<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" id="closeEdit" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit Item</h4>
            </div>
            
            <div class="modal-body">
                <form id="editForm" class="editForm" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" id="id" name="id" value="">
                    <div class="form-group">
                        <label>Name</label>
                        @auth
                            <input type="text" class="form-control" id="name" name="name" readonly value="">
                        @else
                            <input type="text" class="form-control" id="name" name="name" value="">
                        @endauth
                    </div>
                    <div class="form-group">
                        <label>Title</label>
                        <input type="text" class="form-control" id="title" name="title" value="">
                        <p class="small text-danger mt-5" id="titleError"></p>
                    </div>
                    <div class="form-group">
                        <label>Body</label>
                        <textarea rows="5" class="form-control" id="message" name="message"></textarea>
                        <p class="small text-danger mt-5" id="messageError"></p>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4">
                            <img class="img-responsive" alt="" src="">
                        </div>
                        <div class="col-md-8 pl-0">
                            <label>Choose image from your computer :</label>
                            <div class="input-group">
                                <input type="text" class="form-control upload-form" value="No file chosen" readonly>
                                <span class="input-group-btn">
                                    <span class="btn btn-default btn-file">
                                        <i class="fa fa-folder-open"></i>&nbsp;Browse <input type="file" name="image" multiple>
                                    </span>
                                </span>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox">Delete image
                                </label>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" id="closeEdit">Close</button>
                    <button type="button" class="btn btn-primary" id="submitEdit">Save changes</button>
                </div>
        </div>
    </div>
</div>
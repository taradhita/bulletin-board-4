<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Delete Data</h4>
            </div>
            <div class="modal-body pad-20">
                <p id="confirm">Are you sure want to delete this item?</p>
            </div>
            <div class="modal-footer">
                <form id = "deleteForm" class="deleteForm">
                    @csrf
                    @method('DELETE')
                    <input type="hidden" id="id" name="id" value="">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-danger" id="submitDelete">Delete</button>
                </form>
            </div>
        </div>
    </div>
</div>
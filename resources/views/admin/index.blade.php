<x-admin.layout>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Dashboard
            <small>Control panel</small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <!-- /.col-xs-12 -->
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h1 class="font-18 m-0">Timedoor Challenge - Level 9</h1>
                    </div>
                    <form method="GET" action="{{ route('admin.index') }}">
                        <div class="box-body">
                            <x-admin.search />
                            @if($bulletins->count())
                                <x-admin.table-bulletin :bulletins="$bulletins"/>
                
                                <a href="#" class="btn btn-default mt-5" data-toggle="modal" data-target="#deleteModal" id="deleteChecked">Delete Checked Items</a>
                                <div class="text-center">
                                    {{ $bulletins->links('vendor.pagination.bootstrap-4') }}
                                </div>
                            @endif
                        </div>
                    </form>
                </div>
            </div><!-- /.col-xs-12 -->
        </div>
    </section>
    <!-- /.content -->
</x-admin.layout>
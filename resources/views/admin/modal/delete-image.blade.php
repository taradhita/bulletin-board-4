<x-modal id="deleteImageModal" title="Delete Data">
    <p>Are you sure want to delete this item(s)?</p>
    <x-slot name="footer">
        <form id="deleteImageForm" class="deleteImageForm" enctype="multipart/form-data">
            @csrf
            <input type="hidden" id="itemId">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-danger" id="submitDelImage">Delete</button>
        </form>
    </x-slot>
</x-modal>
<x-modal id="deleteModal" title="Delete Data">
    <p>Are you sure want to delete this item(s)?</p>
    <x-slot name="footer">
        <form id="deleteForm" class="deleteForm">
            @csrf
            @method('DELETE')
            <input type="hidden" id="itemId">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-danger" id="submitDelete">Delete</button>
        </form>
    </x-slot>
</x-modal>
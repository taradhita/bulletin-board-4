<x-modal id="validationModal" title="No item selected">
    <p>Please choose at least 1 item</p>
    <x-slot name="footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </x-slot>
</x-modal>
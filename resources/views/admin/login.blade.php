<html>
    <head>
        <title>Timedoor Challenge - Level 8 | Login</title>
        
        @include('admin/template/style')
        @include('admin/template/script')
        
        <!-- Javascript -->
        <script type="text/javascript" src="{{ asset('js/jquery.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
        <!-- Javascript End -->
    </head>
    
    <body id="login">
        <div class="login-box">
            <div class="login-box-head">
                <h1>Login</h1>
                <p>Please login to continue...</p>
            </div>
            <div class="login-box-body">
            <form method="POST" action="{{ route('admin.login') }}">
                @csrf
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Username" name="username" value="{{ old('username') ?? '' }}">
                    @error('username')
                        <p class="mt-5 small text-danger">*{{ $message }}</p>
                    @enderror
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" placeholder="Password" name="password" value="{{ old('password') ?? '' }}">
                    @error('password')
                        <p class="mt-5 small text-danger">*{{ $message }}</p>
                    @enderror
                </div>
            </div>
            <div class="login-box-footer">
                <div class="text-right">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
            </form>
        </div>
    </body>
    
</html>
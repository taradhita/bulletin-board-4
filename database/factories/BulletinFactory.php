<?php

namespace Database\Factories;

use App\Models\Bulletin;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;

class BulletinFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Bulletin::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name'      => 'test',
            'title'     => $this->faker->text(20),
            'message'   => $this->faker->text(100),
            'user_id'   => 1,
            'image'     => null,
            'password'  => null,
        ];
    }
}

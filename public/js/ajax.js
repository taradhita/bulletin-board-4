$(document).ready(function(){
    let fetchPassword = function(id, json){
        return $.ajax({
            url: id + '/confirm-password',
            type: 'POST',
            data: json,
            dataType: 'json',
        });
    }

    function passwordJson(id, pass, action){
        return {
            '_token': $('meta[name=_token]').attr('content'), 
            'id': id, 
            'passwordConfirmation': pass, 
            'action': action
        }
    }

    let fetchData = function(id, action){
        return $.ajax({
            url : id + '/' + action,
            type: "GET",
            data: {'id':id},
        });
    }

    function getEditValue(data) {
        $('#id').val(data.id);
        $('#name').val(data.name); 
        $('#title').val(data.title); 
        $('#message').val(data.message);
        let src =  data.image_thumbnail;
        $('div#editModal img.img-responsive').attr('src', src);
    }

    function showModalForm(action, id){
        if($('#' + action + 'ValidForm').length===0){
            $('.modal-body').append(`
                <form class="form-inline mt-50" method="post" id="` + action + `ValidForm">    
                    <input type='hidden' name='id' value=` + id + `>
                    <div class='form-group'>
                        <label>Password</label>
                        <input type='password' name='passwordConfirmation' class='form-control' id='passwordConfirm`+ action[0].toUpperCase() + action.slice(1) + id +`'>
                    </div>
                </form>
            `);
        } else {
            $('#'+ action +'ValidForm').show();
            $('div#' + action + 'Modal input[name="id"]').val(id);
            $('div#' + action + 'Modal input[id^=passwordConfirm]').attr('id', 'passwordConfirm'+id)
        }
    }

    let deleteData = function(id){
        return $.ajax({
            url: id + '/destroy',
            type: 'DELETE',
            data: {'_token': $('meta[name=_token]').attr('content'), 'id':id},
        });
    }

    $('body').on('click', '#edit', function (e) {
        e.preventDefault();
        let id   = $(this).data('id');
        let pass = $(`input[id='passwordConfirmId${id}']`).val();

        if($('#deleteValidForm').length) $('#deleteValidForm').remove(); 
        
        let postPassword = fetchPassword(id, passwordJson(id,pass,"edit"));

        $.when(postPassword).done(function(data) {
            $('#editModal').modal('show');
            if ($('.editForm').css("display", "none")) $('.editForm').show();
            if ($('button#submitEdit').css("display", "none")) $('button#submitEdit').show();
            if ($('div#editModal .modal-body > p#passError, #editValidForm, button#editPassModal').length !== 0) $('div#editModal .modal-body > p, #editValidForm, button#editPassModal').remove();

            let edit = fetchData(id, 'edit');
            $.when(edit).done(function(data){
                console.log(data);
                if ($('.deleteForm').css("display", "none")) $('.deleteForm').show();
                if(data.image === null) $('.checkbox').hide(); else $('.checkbox').show();
                getEditValue(data);
            });

        }).fail(function(data){
            let errorParse = $.parseJSON(data.responseText);

            $('#editModal').modal('show');
            $('.editForm').hide();
            $('button#submitEdit').hide();

            if ($('div#editModal .modal-body > p#passError').length === 0) {
                $('.modal-body').append("<p class='small text-danger mt-5' id='passError'>" + errorParse.message + '</p>');
            } else $('p#passError').html(errorParse.message);

            if (errorParse.status === 'failed'){
                showModalForm('edit', id);
                if ($('button#submitEdit').css("display", "none") &&  ($('button#editPassModal').css("display", "none") || $('button#editPassModal').length < 1) ){
                    $('#editModal .modal-footer').append('<button type="button" class="btn btn-primary" id="editPassModal" data-id="'+id+'" >Submit</button>')
                }
            } else if (errorParse.status === 'empty'){
                $('div#editModal .modal-body > form#editValidForm, button#editPassModal').hide();
            }
        });
    });

    $('div#editModal').on('click', '#editPassModal', function (e) {
        e.preventDefault();
        let id = $(this).data('id');
        let pass = $(`input[id='passwordConfirmEdit${id}']`).val();
        let postPassword = fetchPassword(id, passwordJson(id,pass,"edit"));

        $.when(postPassword).done(function(data) {
            $('div#editModal .modal-body > p, #editValidForm, button#editPassModal').remove();
            $('.editForm').show();
            $('button#submitEdit').show();
            let edit = fetchData(id, "edit");
            $.when(edit).done(function(data){
                console.log(data);
                if ($('.deleteForm').css("display", "none")) $('.deleteForm').show();
                if(data.image === null) $('.checkbox').hide(); else $('.checkbox').show();
                getEditValue(data);

            }).fail(function(data){
                let errorParse = $.parseJSON(data.responseText);
                $('#passError').html(errorParse.message);
            });
        });
    });

    $('div#editModal').on('keypress','input[id^=passwordConfirm]', function(e) {
        if (e.which == '13') {
            e.preventDefault();
            $('#editPassModal').click();
        } 
    });

    $('#submitEdit').click(function(e){
        e.preventDefault();
        let id      = $("#id").val();
        let editForm = $('#editForm')[0];
        let formData = new FormData(editForm);
        if ($('.checkbox input').is(':checked')) {
            formData.append('isDelete', true);
        }
        console.log(...formData);
        $.ajax({
            url : id + '/update',
            type: 'POST',
            enctype: 'multipart/form-data',
            data: formData,
            processData: false,
            contentType: false,
            success: function(data){
                $('#editModal').modal('hide');
                window.location.reload(true);
            },
            error: function(data) {
                let errorParse = $.parseJSON(data.responseText);
                console.log(errorParse);
                $('#titleError').html(errorParse.errors.title ? ('*' + errorParse.errors.title) : null);
                $('#messageError').html(errorParse.errors.message ? ('*' + errorParse.errors.message) : null);
            }
        });
    });

    $('body').on('click', '#delete', function (e) {
        e.preventDefault();
        let id   = $(this).data('id');
        let pass = $(`input[id='passwordConfirmId${id}']`).val();
        if($('#editValidForm').length) $('#editValidForm').remove(); 
        let postPassword = fetchPassword(id, passwordJson(id,pass,"delete"));

        $.when(postPassword).done(function(data) {
            $('#deleteModal').modal('show');
            if($('p#confirm').css("display", "none")) $('p#confirm').show();
            if ($('.deleteForm').css("display", "none")) $('.deleteForm').show();
            if ($('div#deleteModal .modal-body > p#passError, #deleteValidForm, button#deletePassModal,button#close').length !== 0) {
                $('div#deleteModal .modal-body > p#passError, #deleteValidForm, button#deletePassModal, button#close').remove();
            }

            let getDelete = fetchData(id, "delete");
            $.when(getDelete).done(function(data){
                console.log(data);
                $('#id').val(data.id); 
            })
        }).fail(function (data) {
            let errorParse = $.parseJSON(data.responseText);

            $('#deleteModal').modal('show');
            $('.deleteForm').hide();
            $('p#confirm').hide();
            if ($('div#deleteModal .modal-footer .deleteForm').css("display", "none") && $('#close').length<1) {
                $('div#deleteModal .modal-footer')
                    .append(`<button type="button" class="btn btn-default" id="close" data-dismiss="modal">Cancel</button>`);
            }

            if ($('div#deleteModal .modal-body > p#passError').length === 0) {
                $('.modal-body').append("<p class='small text-danger mt-5' id='passError'>" + errorParse.message + '</p>');
            } else $('p#passError').html(errorParse.message);

            if (errorParse.status === 'failed') {
                showModalForm('delete', id);

                if(($('button#deletePassModal').length<1)) {
                    $('div#deleteModal .modal-footer').append('<button type="button" class="btn btn-primary" id="deletePassModal" data-id="'+id+'">Submit</button>');
                } else $('button#deletePassModal').show();
            } else if (errorParse.status === 'empty'){
                $('div#deleteModal .modal-body > form#deleteValidForm, button#deletePassModal').hide();
            } 
        });
    });

    $('div#deleteModal').on('click', '#deletePassModal', function (e) {
        e.preventDefault();
        let id           = $(this).data('id');
        let pass         = $(`div#deleteModal input[id='passwordConfirmDelete${id}']`).val();
        let postPassword = fetchPassword(id, passwordJson(id,pass,"delete"));

        $.when(postPassword).done(function (){
            if($('p#confirm').css("display", "none")) $('p#confirm').show();
            if ($('.deleteForm').css("display", "none")) $('.deleteForm').show();
            if ($('div#deleteModal .modal-body > p#passError, #deleteValidForm, button#deletePassModal,button#close').length != 0) {
                $('div#deleteModal .modal-body > p#passError, #deleteValidForm, button#deletePassModal, button#close').remove();
            }

            var getDelete = fetchData(id, "delete");
            $.when(getDelete).done(function(data){
                console.log(data);
                $('#id').val(data.id); 
            })
        }).fail(function (data){
            let errorParse = $.parseJSON(data.responseText);
            $('#passError').html(errorParse.message);
        });
    });

    $('div#deleteModal').on('keypress','input[id^=passwordConfirm]', function(e) {
        if (e.which == '13') {
            e.preventDefault();
            $('#deletePassModal').click();
        } 
    });

    $('#submitDelete').click(function(e) {
        e.preventDefault();
        let id          = $("#id").val();
        let itemsCount  = $("input[name='itemsCount']").length;
        let page        = parseInt(location.href.split( "/?page=" )[1]) ?? 1;
        let destroy     = deleteData(id);

        $.when(destroy).done(function(data){
            $('#deleteModal').modal('hide');
            if(itemsCount <= 1 && page > 1) {
                window.location.replace(location.origin + "?page=" + (page-1).toString())
            } else window.location.reload(true);
        })
    });
});
$(function () {
    let deleteData = function(id, url){
        return $.ajax({
            url: '/admin/' + url,
            type: 'DELETE',
            data: {'_token': $('meta[name=_token]').attr('content'), 'id': id},
        });
    }

    let deleteImage = function(id, data){
        return $.ajax({
            url : '/admin/' + id + '/delete-image',
            type: 'POST',
            enctype: 'multipart/form-data',
            data: data,
            processData: false,
            contentType: false,
        });
    }

    function clickButtonId(buttonId) {
        $('body').on('click', buttonId, function(e) {
            e.preventDefault();
            let id = $(this).data('id');
            $('#itemId').val(id);
        });
    }

    $('#deleteChecked').on('click', function(e) {
        e.preventDefault();
        var ids = [];
        $('.checkboxItem:checked').each(function(){
            ids.push($(this).attr('data-id'));
        })
        $('#itemId').val(ids);
        if (ids.length === 0) {
            e.stopPropagation();
            $('#validationModal').modal('show');
        }
    });

    clickButtonId('#deleteItem');
    clickButtonId('#deleteImage');

    $('#submitDelete').on('click', function(e) {
        e.preventDefault();
        let id = $('#itemId').val();
        let url = ((id.split(',').map(Number)).length) > 1 ? 'delete-multiple' : (id + '/destroy');
        let destroy = deleteData(id, url);

        $.when(destroy).done(function(data){
            $('#deleteModal').modal('hide');
            window.location.reload(true);
        });
    });

    $('#submitDelImage').on('click', function(e) {
        e.preventDefault();
        let id = $('#itemId').val();
        let deleteForm = $('#deleteImageForm')[0];
        let formData = new FormData(deleteForm);
        formData.append('isDelete', true);
        formData.set('image', null);
        let imageDelete = deleteImage(id, formData);
        
        $.when(imageDelete).done(function(data){
            $('#deleteImageModal').modal('hide');
            window.location.reload(true);
        });
    });

});
function clickAll(source) {
    let checkboxes = document.querySelectorAll('input[type="checkbox"]');
    for (let checkbox = 0; checkbox < checkboxes.length; checkbox++) {
        if (checkboxes[checkbox] != source)
            checkboxes[checkbox].checked = source.checked;
    }
}
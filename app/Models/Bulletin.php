<?php

namespace App\Models;

use App\Http\Traits\ImageTrait;
use App\Http\Traits\SoftDeleteTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;

class Bulletin extends Model
{
    use HasFactory;
    use ImageTrait;
    use SoftDeleteTrait;

    protected $fillable     = ['title', 'message', 'image', 'password', 'name', 'user_id'];
    protected $appends      = ['image_thumbnail'];
    protected $defaultImage = "http://via.placeholder.com/500x500";

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = $value ? Hash::make($value) : null;
    }

    public function scopeFilter($query, array $filters)
    {
        $query->where('title', 'like', '%' . ($filters['title'] ?? '') . '%')
              ->where('message', 'like', '%' . ($filters['body'] ?? '') . '%')
              ->when($filters['imageOption'] ?? false, fn($query) => 
                        $query->when($filters['imageOption'] == 'yes', function ($query) {
                            $query->whereNotNull('image');
                        })->when($filters['imageOption'] == 'no', function ($query) {
                            $query->whereNull('image');
                        })
                    )
                ->when($filters['statusOption'] ?? false, fn($query) => 
                    $query->when($filters['statusOption'] == 'yes', function ($query) {
                        $query->whereNull('deleted_at');
                    })->when($filters['statusOption'] == 'no', function ($query) {
                        $query->whereNotNull('deleted_at');
                    })
                );
    }
    
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

}

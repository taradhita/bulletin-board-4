<?php

namespace App\Http\Traits;

use Intervention\Image\Facades\Image as Image;
use Illuminate\Support\Facades\File;
use Illuminate\Database\Eloquent\Model;

trait ImageTrait
{
    protected $uploadDir      = '/storage/uploads/';
    protected $thumbnailWidth = 200;
    protected $file;

    protected static function bootImageTrait()
    {
        static::creating(function(Model $model) {
            if ($model->file) {
                $model->storeAllImages($model->file);
            }
        });

        static::updating(function(Model $model) {
            if ($model->isDirty('image')) {
                $model->deleteFile($model->getOriginal('image'), $model->getOriginal('image_thumbnail'));

                if (!request()->isDelete) {
                    $model->storeAllImages($model->file);
                }
            }
        });

        static::deleting(function(Model $model){
            $model->deleteFile($model->image, $model->image_thumbnail);
        });
    }

    public function storeAllImages($file)
    {
        $this->createDirectory();
        $this->storeFile($file);
        $this->storeThumbnail();
    }

    public function storeFile($file)
    {
        $fileName  = $this->attributes['image'];
        $file->storeAs(('uploads/'. $this->getTable()), $fileName, 'public');
    }

    public function storeThumbnail()
    {
        $this->resizeThumbnail($this->attributes['image'], $this->thumbnailWidth);
    }

    public function deleteFile($image, $thumbnail)
    {
        if ($image != null) {
            File::delete([
                public_path($image),
                public_path($thumbnail)
            ]);
        }
    }

    /**
     * Default image each item
     * 
     */
    public function getDefaultImage(): string
    {
        return $this->defaultImage;
    }
    
    public function setImageAttribute($value)
    {
        $this->file = $value;

        return $this->attributes['image'] = (!empty($value) && !request()->isDelete) ? $this->getFileName($value) : null;
    }

    public function getImageAttribute()
    {
        return $this->attributes['image'] ? $this->getDirName() . '/' . $this->attributes['image'] : $this->getDefaultImage();
    }

    public function getImageThumbnailAttribute()
    {
        return $this->attributes['image'] ? $this->getDirName() . '/thumbnail/' . $this->attributes['image'] : $this->getDefaultImage();
    }

    public function getFileName($file)
    {
        return 'image_' . time() . '.' . $file->getClientOriginalExtension();
    }

    public function getDirName()
    {
        return $this->uploadDir . $this->getTable();
    }

    public function getStorageDirectory()
    {
        return public_path($this->getDirName());
    }

    public function createDirectory()
    {
        if (!is_dir($this->getStorageDirectory())) {
            File::makeDirectory($this->getStorageDirectory() . '/thumbnail', 0755, true);
        }
    }

    public function resizeThumbnail($path, $width)
    {
        Image::make($this->getStorageDirectory() . '/' . $path)->resize($width, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save($this->getStorageDirectory() . '/thumbnail/' . $path);
    }

}
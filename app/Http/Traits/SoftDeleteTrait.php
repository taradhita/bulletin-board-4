<?php

namespace App\Http\Traits;

use Illuminate\Database\Eloquent\SoftDeletes;

trait SoftDeleteTrait
{
    use SoftDeletes {
        SoftDeletes::runSoftDelete as defaultRunSoftDelete;
    }

    /*
    * Update field during soft delete
    *
    */
    protected function runSoftDelete()
    {
        $query = $this->setKeysForSaveQuery($this->newModelQuery());

        $time = $this->freshTimestamp();

        $columns = [$this->getDeletedAtColumn() => $this->fromDateTime($time)];

        $this->{$this->getDeletedAtColumn()} = $time;

        if ($this->timestamps && ! is_null($this->getUpdatedAtColumn())) {
            $this->{$this->getUpdatedAtColumn()} = $time;

            $columns[$this->getUpdatedAtColumn()] = $this->fromDateTime($time);
        }

        $columns['image'] = null;

        $query->update($columns);
    }
}
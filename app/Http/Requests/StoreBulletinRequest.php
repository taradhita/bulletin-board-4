<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreBulletinRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name'      => 'nullable',
            'user_id'   => 'nullable',
            'title'     => 'required|string|between:10,32',
            'message'   => 'required|string|between:10,200',
            'image'     => 'nullable|mimes:jpeg,jpg,png,gif|max:1024',
            'password'  => 'nullable|digits:4'
        ];
        
        return $rules;
    }
}

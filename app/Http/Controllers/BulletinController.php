<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Bulletin;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\StoreBulletinRequest;
use Illuminate\Support\Facades\Auth;

class BulletinController extends Controller
{

    protected $user;

    public function __construct()
    {
        $this->middleware(['guestOrVerified']);
        $this->middleware(function($request, $next){
            if (Auth::check()) {
                $this->user = Auth::user();
            }

            return $next($request);
        });
    }

    public function index()
    {
        $bulletins = Bulletin::latest()->paginate(10);
        $page      = json_decode($bulletins->toJSON());

        return view('index', compact('bulletins','page'));
    }

    public function store(StoreBulletinRequest $request)
    {
        $bulletin = new Bulletin($request->validated());
        
        $bulletin->user_id = optional($this->user)->id;

        $bulletin->save();

        return redirect()->back()
                         ->with('success', 'Item created successfully');
    }

    public function confirmPassword(Request $request, Bulletin $bulletin)
    {
        $actions  = ['edit' => 'edited', 'delete' => 'deleted'];
        $response = [];

        if ($this->user){
            $response = [
                'status' => 'success',
            ];
            $statusCode = 200;
        } else{
            if (empty($bulletin->password)){
                $response = [
                    'status'    => 'empty',
                    'message'   => "This message can't be " . $actions[$request->input('action')] . ", because this message's password has not been set",
                ];
                $statusCode = 422;
            } else {
                if (Hash::check($request->passwordConfirmation, $bulletin->password)) {
                    $response = [
                        'status' => 'success',
                    ];
                    $statusCode = 200;
                } else {
                    $response = [
                        'status'    => 'failed',
                        'message'   => 'The passwords you entered do not match. Please try again.',
                    ];
                    $statusCode = 422;                            
                }
            }
        }
        return response()->json($response, $statusCode);   
    }

    public function confirmDelete(Bulletin $bulletin)
    {
        return response()->json($bulletin);
    }

    public function destroy(Bulletin $bulletin)
    {
        $bulletin->delete();

        return response()->json([ 'success' => true ]);
    }

    public function edit(Bulletin $bulletin)
    {
        return response()->json($bulletin);
    }

    public function update(Bulletin $bulletin, StoreBulletinRequest $request)
    {
        $bulletin->update($request->validated());

        return response()->json([ 'success' => true ]);
    }
}

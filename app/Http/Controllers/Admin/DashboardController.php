<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Bulletin;

class DashboardController extends Controller
{
    public function index(Request $request)
    {
        $bulletins = Bulletin::withTrashed()->filter($request->all())->paginate(20)->withQueryString();
        return view('admin.index', compact('bulletins'));
    }

    public function deleteImage(Bulletin $bulletin, Request $request)
    {
        $bulletin->update($request->all());
        return response()->json([ 'success' => true ]);
    }

    public function destroy(Bulletin $bulletin)
    {
        $bulletin->delete();
        return response()->json([ 'success' => true ]);
    }

    public function deleteMultiple(Request $request)
    {
        $ids = $request->id;
        Bulletin::find($ids)->each(function($bulletin){
            $bulletin->delete();
        });
        return response()->json([ 'success' => true ]);
    }

    public function restore($id)
    {
        Bulletin::onlyTrashed()->where('id', $id)->restore();
        return redirect()->back();
    }

}

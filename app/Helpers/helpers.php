<?php

use Carbon\Carbon;

function customDate($date, $format) {
    return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format($format);
}